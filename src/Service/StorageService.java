package Service;

import java.util.ArrayList;
import java.util.List;

import Domain.Person;

public class StorageService {
	
	private List<Person> db = new ArrayList<Person>();
	
	public void add(Person person){
		Person newPerson = new Person(person.getName(), person.getSurname(), person.getEmail(), person.getEmailValidate(), person.getOccupation());
		db.add(newPerson);
	}
	
	public List<Person> getAllPersons(){
		return db;
	}

}
