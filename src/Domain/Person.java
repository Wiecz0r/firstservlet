package Domain;

public class Person {
	private String name;
	private String surname;
	private String email;
	private String emailValidate;
	private String occupation;
	
	public Person(String name, String surname, String email, String emailValidate, String occupation){
		this.name = name;
		this.surname = surname;
		this.email = email;
		this.emailValidate = emailValidate;
		this.occupation = occupation;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEmailValidate() {
		return emailValidate;
	}

	public void setEmailValidate(String emailValidate) {
		this.emailValidate = emailValidate;
	}

	public String getOccupation() {
		return occupation;
	}

	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}
	
}